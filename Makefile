RUNE?=THOR.RUNE
PUBKEY=thorpub1addwnpepqfgfxharps79pqv8fv9ndqh90smw8c3slrtrssn58ryc5g3p9sx856x07yn

build:
	pip3 install -r requirements.txt

deploy-and-run:
	python3 main.py --deploy true --pubkey ${PUBKEY}

run:
	python3 main.py --pubkey ${PUBKEY}

.PHONY: build run
